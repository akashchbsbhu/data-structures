// page41_19.c
// To know weather a number or a char
// if a char then turn it into other version
#include<stdio.h>


int main()
{
	char c;
	printf("Enter a value: ");
	scanf("%c", &c);
	if(c>=48 && c<=57)
		printf("Digit!\n");
	else
	{
		printf("Character!\n");
		if(c >= 65 && c <=90)
			printf("%c\n", c+32);
		else if(c >= 97 && c <=122)
			printf("%c\n", c-32);
	}
	
	return 0;
	
}
