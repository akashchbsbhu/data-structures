// Program to convert the lover case chars of a string to upper case.c
#include<stdio.h>
#include<ctype.h> // to use toupper and tolower, can be used for each char only

int main()
{
	char str[100], upper_str[100];
	int i=0;
	
	printf("Enter the string: ");
	gets(str);
	while(str[i]!='\0')
	{
		if(str[i]>='a' && str[i]<='z')
		{
			str[i]=toupper(str[i]);
			// or
			// str[i] = str[i] - 32;
		}
		i++;
	}
	printf("%s\n", str);
	
	return 0;
}
