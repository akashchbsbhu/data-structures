// page 40_1.c
// 1. Write a program to read 10 integers. Display these
// numbers by printing three numbers in a line
// separated by commas. 
#include<stdio.h>

int main()
{
	int a[10], i, n=10;
	for(i=0;i<n;i++)
	{
		scanf("%d", &a[i]);
	}
	
	for(i=0;i<n;i++)
	{
		if(i%3 == 0)
			printf("\n");
		printf("%d, ", a[i]);
	}
	
	return 0;
	
}
