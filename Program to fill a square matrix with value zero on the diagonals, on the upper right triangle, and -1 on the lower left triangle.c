// Program to fill a square matrix with value zero on the diagonals, on the upper right triangle, and -1 on the lower left triangle.c
#include<stdio.h>

void read_matrix(int mat[5][5], int);
void display_matrix(int mat[5][5], int);

int main()
{
	int row;
	int mat1[5][5];
	printf("Enter the number of rows and columns of the matrix:\n");
	scanf("%d", &row);
	read_matrix(mat1, row);
	display_matrix(mat1, row);
	
	return 0;
}

void read_matrix(int mat[5][5], int r)
{
	int i, j;
	for(i=0;i<r;i++)
	{
		for(j=0;j<r;j++)
		{
			if(i==j)
				mat[i][j]=0;
			else if(i>j)
				mat[i][j]=-1;
			else
				mat[i][j]=1;
		}
	}
}

void display_matrix(int mat[5][5], int r)
{
	int i, j;
	for(i=0; i<r; i++)
	{

		for(j=0; j<r; j++)
			printf("%d\t", mat[i][j]);	
		printf("\n");
	}
}










//
