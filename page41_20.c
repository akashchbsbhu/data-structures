// page41_20.c
#include<stdio.h>

int main()
{
	int i=0, n, sum=0;
	
	printf("Enter a number: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
		sum+=i;
	
	printf("The sum is %d\n", sum);
	
	return 0;
}
