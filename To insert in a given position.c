// To insert in a given position.c
#include<stdio.h>

int main()
{
	int i, n, pos, arr[10], num;
	printf("Enter for n value: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
		arr[i]=i;
	
	printf("Enter a value for num: ");
	scanf("%d", &num);
	printf("Enter a num of position: ");
	scanf("%d", &pos);
	
	for(i=n-1;i>=pos;i--)
		arr[i+1]=arr[i];
	
	arr[pos]=num;
	n+=1;
	
	for(i=0;i<n;i++)
	{
		printf("arr[%d]=",i);
		printf("%d\n", arr[i]);	
	}
	
	return 0;
		
}
