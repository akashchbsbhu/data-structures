// Insert into an array.c
#include<stdio.h>

int main()
{
	int arr[10], num, n, pos, i, j;
	printf("Enter a value for n: ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
	{
		printf("Enter for arr[%d]=", i);
		scanf("%d", &arr[i]);
	}
	
	printf("Enter number to be inserted: ");
	scanf("%d", &num);
	printf("Enter position to take the input: ");
	scanf("%d", &pos);
	
	for(i=n-1;i>=pos;i--)
	{
		arr[i+1]=arr[i];
	}
	arr[pos]=num;
	n=n+1;
	for(i=0;i<n;i++)
	{
		printf("arr[%d]=%d\n", i, arr[i]);
	}
	
	return 0;
}
