// Two variables with same names with different data types.c
#include<stdio.h>

int main()
{
	int azim =17;
	float azim =1.17; // gives an error as saying it was delared before
	
	printf("azim int data value: %d\n", azim);
	printf("azim float data value: %f\n", azim);
	
	return 0;
	
}
