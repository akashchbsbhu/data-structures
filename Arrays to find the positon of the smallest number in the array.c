// Arrays to find the positon of the smallest number in the array.c
// Author: Azimjon Kamolov
// Operations on Arrays
// * Traversing an array <<<
// * Inserting an element in an array
// * Searching an element in an array
// * Deleting an element form an array
// * Merging two arrays
// * Sorting an array in ascending or descending order
#include<stdio.h>

int main()
{
	int i, n, arr[20], small, pos;
	printf("Enter the number of elements in the array: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
	{
		scanf("%d", &arr[i]);
	}
	small = arr[0];
	pos=0;
	for(i=1;i<n;i++)
	{
		if(arr[i]<small)
		{
			small=arr[i];
			pos=i;
		}
	}
	printf("The smallest element in the array: %d\n", small);
	printf("The position of the smalles element is in the array: %d", pos);
	
	return 0;
}
