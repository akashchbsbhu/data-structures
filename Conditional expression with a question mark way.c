// Conditional expression with a question mark way.c
#include<stdio.h>

int main()
{
	int result, a, b;
	
	printf("Enter two integer values please: ");
	scanf("%d %d", &a, &b);
	result = a > b ? a : b; // if the condition is true first one runs if not the second one runs
	
	printf("The largest number is %d\n", result);
	
	return 0; // to give OS 0 value
	
}
