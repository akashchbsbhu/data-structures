// Program to insert a string in the main text.c
#include<stdio.h>
#include<string.h>

int main()
{
	char text[50], str[20], ins_text[20];
	int i=0, j=0, k=0, pos;
	
	printf("Enter the main text: ");
	gets(text);
	printf("Enter the string to be inserted: ");
	gets(str);
	printf("Enter the position at which the string has to be instertrd: ");
	scanf("%d", &pos);
	while(text[i]!='\0')
	{
		if(i==pos)
		{
			while(str[k]!='\0')
			{
				ins_text[j]=str[k];
				j++;
				k++;
			}
		}
		else
		{
			ins_text[j]=text[i];
			j++;
		}
		i++;
	}
	
	ins_text[j]='\0';
	printf("The new string is: ");
	puts(ins_text);
	
	return 0;
}
