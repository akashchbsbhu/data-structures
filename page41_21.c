// page41_21.c
// Add all odd nums from m to n
#include<stdio.h>

int main()
{
	int m, n, sum=0;
	scanf("%d%d", &m, &n);
	while(m<=n)
	{
		if(m%2==0)
		{
			sum+=m;
		}
		m++;
	}
	
	printf("The sum is %d\n", sum);
	
	return 0;
}
