// pointersandarrays.c
#include<stdio.h>

int main()
{
	int a[]={1,2,3,4,5};
	int *p=&a[0];
	printf("the add of a[0]is %p\n", p);
	p++;
	printf("the add of a[0]is %p\n", p);
	printf("Address of array: %p %p\n", &a[2], &a); // showes the address of them
	printf("The value of the second element of the array is %d\n", *p); // 2 is an output
	
	return 0;
}
